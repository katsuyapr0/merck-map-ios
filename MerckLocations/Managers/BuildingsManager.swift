//
//  BuildingsManager.swift
//  MerckLocations
//
//  Created by Diego Vidal on 25/04/16.
//  Copyright © 2016 Diego Vidal. All rights reserved.
//

import Foundation
import SwiftyJSON

enum CurrentlyActiveSearch {
    case StartBuilding
    case FinishBuilding
}

struct BuildingsManager {
    
    var buildings: [Building]
    private var startBuilding: Building?
    private var finishBuilding: Building?
    var startBuildingsSearchResults: [Building] = []
    var finishBuildingsSearchResults: [Building] = []
    var currentlyActiveSearch: CurrentlyActiveSearch?
    
    var buildingsCount: Int {
        return buildings.count
    }
    
    init() {
        guard let fileName = NSBundle.mainBundle().pathForResource("merck_buildings_new", ofType: "json") else { fatalError() }
        guard let data = NSData(contentsOfFile: fileName) else { fatalError("Error reading geojson file") }
        
        do {
            let jsonDict = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as! [String : AnyObject]
            //print(jsonDict)
            let jsonObject = JSON(jsonDict)
            let geometriesArray = jsonObject["features"].object as! [[String: AnyObject]]
            let filteredGeometriesArray = geometriesArray.filter({ geometryDict in
                let geometryJson = JSON(geometryDict)
                if let geometryType = geometryJson["geometry"]["type"].string where geometryType == "MultiPolygon", let buildingName = geometryJson["properties"]["Name"].string where buildingName != " " {
                    return true
                } else {
                    return false
                }
            })
            buildings = Building.buildingsFromJsonArray(filteredGeometriesArray)
            
        } catch {
            print("Error trying to create json objecto from data: \(error)")
            fatalError()
        }
    }
    
    //Initializer for merk-germany.json
    /*init() {
        guard let fileName = NSBundle.mainBundle().pathForResource("Merck-Germany", ofType: "json") else { fatalError() }
        guard let data = NSData(contentsOfFile: fileName) else { fatalError("Error reading geojson file") }
        
        do {
            let jsonDict = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as! [String : AnyObject]
            print(jsonDict)
            let jsonObject = JSON(jsonDict)
            let geometriesArray = jsonObject["features"].object as! [[String: AnyObject]]
            let filteredGeometriesArray = geometriesArray.filter({ geometryDict in
                let geometryJson = JSON(geometryDict)
                if let geometryType = geometryJson["geometry"]["type"].string where geometryType == "Polygon" {
                    return true
                } else {
                    return false
                }
            })
            buildings = Building.buildingsFromJsonArray(filteredGeometriesArray)
            
        } catch {
            print("Error trying to create json objecto from data: \(error)")
            fatalError()
        }
    }*/
    
    func getStartBuilding() -> Building? {
        return startBuilding
    }
    
    func getFinishBuilding() -> Building? {
        return finishBuilding
    }
    
    mutating func setStartBuilding(building: Building) {
        startBuilding = building
    }
    
    mutating func setFinishBuilding(building: Building) {
        finishBuilding = building
    }
    
    func buildingAtIndex(index: Int) -> Building {
        return buildings[index]
    }
    
    func searchBuildingsWithString(searchString: String) -> [Building] {
        return buildings.filter { building in
            return building.name.lowercaseString.containsString(searchString.lowercaseString)
        }
    }
    
    subscript(index: Int) -> Building {
        return buildings[index]
    }
}