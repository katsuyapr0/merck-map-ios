//
//  RoutesManager.swift
//  MerckLocations
//
//  Created by Diego Vidal on 25/04/16.
//  Copyright © 2016 Diego Vidal. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps
import SwiftyJSON

enum RoutesDirectionsError: ErrorType {
    case WrongJsonFormat
    case NoData
    case ResponseError
}

enum DVResult<T, U> {
    case success(T)
    case Failure(U)
}

enum RouteType: String {
    case Driving = "driving"
    case Walking = "walking"
}

struct RoutesManager {
    
    static var currentlySelectedRouteType: RouteType = .Walking
    
    static func calculateRouteFrom(startLocation: CLLocationCoordinate2D, to finishLocation: CLLocationCoordinate2D, routeType: RouteType, completion: (result: DVResult<Route, RoutesDirectionsError>) -> Void) {
        
        let originString = "\(startLocation.latitude),\(startLocation.longitude)"
        let destinationString = "\(finishLocation.latitude),\(finishLocation.longitude)"
        let directionsAPI = "https://maps.googleapis.com/maps/api/directions/json?"
        let directionsUrlString = directionsAPI + "&origin=\(originString)&destination=\(destinationString)&mode=\(routeType.rawValue)&key=AIzaSyA5D0Dv1S465gdZc3LvNjCUMJppTxOUazg"
        print("Google Maps Directions Url: \(directionsUrlString)")
        let directionsUrl = NSURL(string: directionsUrlString)
        
        let dataTask = NSURLSession.sharedSession().dataTaskWithURL(directionsUrl!) { (data, response, error) in
            if error == nil {
                if data != nil {
                    do {
                        let jsonDict = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                        print("Success getting Google Maps Directions: \(jsonDict)")
                        guard let routes = jsonDict["routes"] as? NSArray else { fatalError() }
                        if routes.count > 0 {
                            dispatch_async(dispatch_get_main_queue()) {
                                let routeDict = routes.firstObject as! NSDictionary
                                let routeOverviewPolylineDict = routeDict["overview_polyline"] as! NSDictionary
                                let pointsString = routeOverviewPolylineDict["points"] as! String
                                let path = GMSPath(fromEncodedPath: pointsString)
                                let polyline = GMSPolyline(path: path)
                                switch routeType {
                                case .Driving:
                                    polyline.strokeColor = .merckRedColor()
                                case .Walking:
                                    polyline.strokeColor = .merckBlueColor()
                                }
                                polyline.strokeWidth = 4.0
                                
                                let routeJson = JSON(routeDict)
                                let distance = routeJson["legs"][0]["distance"]["text"].string
                                let estimatedTime = routeJson["legs"][0]["duration"]["text"].string
                                
                                completion(result: DVResult.success(Route(polyline: polyline, distance: distance, estimatedTime: estimatedTime)))
                            }
                        }
                        
                    } catch {
                        print("Could not create a dictionary from the google maps directions API: \(error)")
                        dispatch_async(dispatch_get_main_queue()) {
                            completion(result: DVResult.Failure(.WrongJsonFormat))
                        }
                    }
                } else {
                    print("There was no data in the Google Directions API response")
                    dispatch_async(dispatch_get_main_queue(), { 
                        completion(result: DVResult.Failure(.NoData))
                    })
                }
            } else {
                print("Error in the Google Maps Directions API: \(error!.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), { 
                    completion(result: DVResult.Failure(.ResponseError))
                })
            }
        }
        dataTask.resume()
    }
    
}