//
//  AnimationsManager.swift
//  MerckLocations
//
//  Created by Diego Vidal on 26/04/16.
//  Copyright © 2016 Diego Vidal. All rights reserved.
//

import UIKit

struct MapAnimationsManager {
    
    func performMerkLogoAnimation(backgroundView: UIView, logoImageView: UIImageView, overView view: UIView) {
        UIView.animateWithDuration(1.5, animations: {
            backgroundView.alpha = 0.0
            logoImageView.frame = CGRect(x: view.bounds.size.width/2.0 - 45.0, y: view.bounds.size.height - 45.0, width: 90.0, height: 35)
            }) { _ in
                backgroundView.removeFromSuperview()
        }
    }
}