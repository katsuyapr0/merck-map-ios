//
//  DrawingManager.swift
//  MerckLocations
//
//  Created by Diego Vidal on 25/04/16.
//  Copyright © 2016 Diego Vidal. All rights reserved.
//

import Foundation
import GoogleMaps

struct DrawingManager {
    
    var polygonsArray: [GMSPolygon] = []
    var currentStartPolygon: GMSPolygon?
    var currentFinishPolygon: GMSPolygon?
    
    func mapPolygonsFromBuildings(buildings: [Building]) -> [GMSPolygon] {
        return buildings.map { building in
            let path = GMSMutablePath()
            for coord in building.pointsCoordinates {
                path.addCoordinate(coord)
            }
            let polygon = GMSPolygon(path: path)
            polygon.fillColor = .darkGrayColor()
            polygon.strokeColor = .merckGreenColor()
            polygon.strokeWidth = 2.0
            return polygon
        }
    }
    
    func mapPolygonFromBuilding(building: Building) -> GMSPolygon {
        let path = GMSMutablePath()
        for coord in building.pointsCoordinates {
            path.addCoordinate(coord)
        }
        let polygon = GMSPolygon(path: path)
        polygon.fillColor = .darkGrayColor()
        polygon.strokeColor = .whiteColor()
        polygon.strokeWidth = 2.0
        return polygon
    }
    
    func paintPolygon(polygon: GMSPolygon, withColor color: UIColor) {
        polygon.fillColor = color
    }
    
    func getPolygonOfBuilding(building: Building) -> GMSPolygon? {
        return polygonsArray.filter({ polygon in
            if let polygonTitle = polygon.title where polygonTitle == building.name {
                return true
            } else {
                return false
            }
        }).first
    }
}