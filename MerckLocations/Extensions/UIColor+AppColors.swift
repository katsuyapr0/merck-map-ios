//
//  UIColor+AppColors.swift
//  MerckLocations
//
//  Created by Diego Vidal on 26/04/16.
//  Copyright © 2016 Diego Vidal. All rights reserved.
//

import UIKit

extension UIColor {
    static func merckGrayColor() -> UIColor {
        return UIColor(red: 56.0/255.0, green: 61.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    }
    
    static func merckGreenColor() -> UIColor {
        return UIColor(red: 173.0/255.0, green: 204.0/255.0, blue: 57.0/255.0, alpha: 1.0)
    }
    
    static func merckLogoBlueColor() -> UIColor {
        return UIColor(red: 23.0/255.0, green: 92.0/255.0, blue: 167.0/255.0, alpha: 1.0)
    }
    
    static func merckBlueColor() -> UIColor {
        return UIColor(red: 0.0/255.0, green: 128.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    
    static func merckRedColor() -> UIColor {
        return UIColor(red: 255.0/255.0, green: 102.0/255.0, blue: 102.0/255.0, alpha: 1.0)
    }
}