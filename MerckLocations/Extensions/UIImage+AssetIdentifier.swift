//
//  UIImage+AssetIdentifier.swift
//  MerckLocations
//
//  Created by Diego Vidal on 26/04/16.
//  Copyright © 2016 Diego Vidal. All rights reserved.
//

import UIKit

extension UIImage {
    enum AssetIdentifier: String {
        case MerckLogo = "MerkLogoNew"
        case DrivingIcon = "DrivingIcon"
        case WalkingIcon = "WalkingIcon"
    }
    
    convenience init!(assetIdentifier: AssetIdentifier) {
        self.init(named: assetIdentifier.rawValue)
    }
}