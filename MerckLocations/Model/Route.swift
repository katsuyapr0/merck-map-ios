//
//  Route.swift
//  MerckLocations
//
//  Created by Diego Vidal on 26/04/16.
//  Copyright © 2016 Diego Vidal. All rights reserved.
//

import Foundation
import GoogleMaps

struct Route {
    let polyline: GMSPolyline
    let distance: String?
    let estimatedTime: String?
}
