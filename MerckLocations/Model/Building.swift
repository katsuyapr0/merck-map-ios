//
//  Building.swift
//  NewGeoJson
//
//  Created by Diego Vidal on 25/04/16.
//  Copyright © 2016 Diego Vidal. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON

protocol Drawable {
    var fillColor: UIColor { get }
    var borderColor: UIColor { get }
}

extension Drawable {
    var fillColor: UIColor {
        return .greenColor()
    }
    
    var borderColor: UIColor {
        return .purpleColor()
    }
}

struct Building: Drawable, Equatable {
    //let identifier: String
    let name: String
    let entranceLocation: CLLocationCoordinate2D
    let pointsCoordinates: [CLLocationCoordinate2D]
    
    //Initializer for the test file: Buildings.json
    /*init(jsonObject: JSON) {
        self.identifier = jsonObject["_id"].string!
        self.name = jsonObject["name"].string!
        
        let entranceCoordinateArray = jsonObject["entrance_coordinates"].object as! [Double]
        self.entranceLocation = CLLocationCoordinate2D(latitude: entranceCoordinateArray[0], longitude: entranceCoordinateArray[1])
        
        let coordinatesArray = jsonObject["coordinates"].object as! [[Double]]
        self.pointsCoordinates = coordinatesArray.map { coordinateArray in
            CLLocationCoordinate2D(latitude: coordinateArray[0], longitude: coordinateArray[1])
        }
    }*/
    
    init(jsonObject: JSON) {
        self.name = jsonObject["properties"]["Name"].string!
        print("Nameeeee: \(self.name)")
        let coordinatesOuterArray = jsonObject["geometry"]["coordinates"]
        let coordinatesOuterArray2 = coordinatesOuterArray[0][0].object as! [[Double]]
        //let coordinatesArray = coordinatesOuterArray2[0] as! [[Double]]
        self.pointsCoordinates = coordinatesOuterArray2.map { coordinateArray in
            CLLocationCoordinate2D(latitude: coordinateArray[1], longitude: coordinateArray[0])
        }
        self.entranceLocation = self.pointsCoordinates.first!
    }
    
    static func buildingsFromJsonArray(jsonArray: [[String: AnyObject]]) -> [Building] {
        return jsonArray.map{ jsonDict in
            let jsonObject = JSON(jsonDict)
            return Building(jsonObject: jsonObject)
        }
    }
}

func ==(lhs: Building, rhs: Building) -> Bool {
    return lhs.name == rhs.name
}

extension Building: CustomStringConvertible {
    var description: String {
        return "--- Name: \(name), EntranceLocation: \(entranceLocation), PointsCoordinates: \(pointsCoordinates)"
    }
}
