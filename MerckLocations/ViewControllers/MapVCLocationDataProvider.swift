//
//  MapVCDataProvider.swift
//  MerckLocations
//
//  Created by Diego Vidal on 28/04/16.
//  Copyright © 2016 Diego Vidal. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class MapVCLocationDataProvider: NSObject, CLLocationManagerDelegate {
    
    private let mapView: GMSMapView
    
    init(mapView: GMSMapView) {
        self.mapView = mapView
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if case .AuthorizedWhenInUse = status {
            manager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Entered did update location!!!")
        guard let location = locations.first else { return }
        mapView.animateToCameraPosition(GMSCameraPosition.cameraWithLatitude(location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 16))
        manager.stopUpdatingLocation()
    }
}