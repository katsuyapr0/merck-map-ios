//
//  MapViewController.swift
//  MerckLocations
//
//  Created by Diego Vidal on 25/04/16.
//  Copyright © 2016 Diego Vidal. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class MapViewController: UIViewController {
    
    enum TextFieldType: Int {
        case StartLocationTextField = 0
        case FinishLocationTextField
    }
    
    enum RouteButtonType: Int {
        case WalkingButton = 0
        case DrivingButton
    }
    
    @IBOutlet private weak var startLocationDotLabel: UILabel!
    @IBOutlet private weak var finishLocationDotLabel: UILabel!
    @IBOutlet private weak var startLocationTextField: UITextField!
    @IBOutlet private weak var finishLocationTextField: UITextField!
    @IBOutlet private weak var walkingButton: UIButton!
    @IBOutlet private weak var drivingButton: UIButton!
    @IBOutlet private weak var formContainerView: UIView!
    @IBOutlet private weak var resultsTableView: UITableView!
    @IBOutlet private weak var distanceAndEtaLabel: UILabel!
    @IBOutlet var resultsContainerView: UIView!
    var whiteView: UIView!
    var logoImageView: UIImageView!
    lazy var sphereMenu: SphereMenu = {
        let image1 = UIImage(assetIdentifier: .DrivingIcon)
        let image2 = UIImage(assetIdentifier: .DrivingIcon)
        let startImage = UIImage(assetIdentifier: .WalkingIcon)
        let menu = SphereMenu(startPoint: CGPoint(x: 200.0, y: 200.0), startImage: startImage, submenuImages: [image1, image2])
        return menu
    }()
    
    var mapView: GMSMapView!
    var polylinesArray: ContiguousArray<GMSPolyline> = []
    let locationManager = CLLocationManager()
    var buildingsManager = BuildingsManager()
    var drawingManager = DrawingManager()
    lazy var locationDataProvider: MapVCLocationDataProvider = {
        return MapVCLocationDataProvider(mapView: self.mapView)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        mapView = GMSMapView(frame: view.bounds)
        mapView.mapType = kGMSTypeSatellite
        mapView.settings.consumesGesturesInView = false
        mapView.settings.myLocationButton = true
        mapView.delegate = self
        mapView.myLocationEnabled = true
        for i in 0..<buildingsManager.buildingsCount {
            let building = buildingsManager.buildingAtIndex(i)
            let polygon = drawingManager.mapPolygonFromBuilding(building)
            polygon.title = building.name
            polygon.tappable = true
            drawingManager.polygonsArray.append(polygon)
            polygon.map = mapView
        }
        view.insertSubview(mapView, belowSubview: formContainerView)
        //view.addSubview(sphereMenu)
        
        locationManager.delegate = locationDataProvider
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
            print("I'm already authorized to access the user's location")
            locationManager.startUpdatingLocation()
        } else {
            print("I'm not authorized to access user's location")
            locationManager.requestWhenInUseAuthorization()
        }
        
        //////////////////////////////////////////////
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureDetected(_:)))
        mapView.addGestureRecognizer(panGesture)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        performSelector(#selector(animateMerckLogo), withObject: nil, afterDelay: 1.0)
        UIView.animateWithDuration(1.0, delay: 2.0, options: .CurveEaseOut, animations: {
            //self.formContainerView.alpha = 1.0
            self.formContainerView.transform = CGAffineTransformIdentity
            }) { _ in }
    }
    
    func setupUI() {
        startLocationDotLabel.text = "●"
        startLocationDotLabel.textColor = .merckLogoBlueColor()
        finishLocationDotLabel.text = "●"
        finishLocationDotLabel.textColor = .merckGreenColor()
        
        startLocationTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        startLocationTextField.layer.borderWidth = 1.0
        startLocationTextField.layer.cornerRadius = 20.0
        startLocationTextField.attributedPlaceholder = NSAttributedString(string: "Start Location", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        finishLocationTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        finishLocationTextField.layer.borderWidth = 1.0
        finishLocationTextField.layer.cornerRadius = 20.0
        finishLocationTextField.attributedPlaceholder = NSAttributedString(string: "Finish Location", attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        walkingButton.layer.cornerRadius = 20.0
        drivingButton.layer.cornerRadius = 20.0
        
        startLocationTextField.addTarget(self, action: #selector(locationTextFieldEdited(_:)), forControlEvents: .EditingChanged)
        finishLocationTextField.addTarget(self, action: #selector(locationTextFieldEdited(_:)), forControlEvents: .EditingChanged)
        
        resultsContainerView.frame = CGRect(x: 0.0, y: formContainerView.frame.origin.y + formContainerView.frame.size.height, width: view.bounds.size.width, height: 120.0)
        resultsContainerView.hidden = true
        resultsContainerView.layer.shadowOpacity = 0.8
        resultsContainerView.layer.shadowRadius = 3.0
        resultsContainerView.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        resultsContainerView.layer.shadowColor = UIColor.blackColor().CGColor
        view.addSubview(resultsContainerView)
        
        /////////////////////////////////////////////////////////
        
        whiteView = UIView(frame: view.bounds)
        whiteView.backgroundColor = UIColor.whiteColor()
        view.addSubview(whiteView)
        
        let logoHeight: CGFloat = 128.0
        logoImageView = UIImageView(frame: CGRect(x: 68.0, y: view.center.y - logoHeight/2.0, width: view.bounds.size.width - 136.0, height: logoHeight))
        logoImageView.image = UIImage(assetIdentifier: .MerckLogo)
        logoImageView.contentMode = .ScaleAspectFit
        view.addSubview(logoImageView)
        
        /////////////////////////////////////////////////////////
        
        //formContainerView.alpha = 0.0
        formContainerView.transform = CGAffineTransformMakeTranslation(0.0, -formContainerView.bounds.size.height)
        formContainerView.layer.shadowColor = UIColor.blackColor().CGColor
        formContainerView.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        formContainerView.layer.shadowRadius = 5.0
        formContainerView.layer.shadowOpacity = 0.8
    }
    
    func animateMerckLogo() {
        MapAnimationsManager().performMerkLogoAnimation(whiteView, logoImageView: logoImageView, overView: view)
    }
    
    func locationTextFieldEdited(textField: UITextField) {
        guard let textFieldType = TextFieldType(rawValue: textField.tag) else { return }
        
        if let text = textField.text where text.isEmpty {
            resultsContainerView.hidden = true
        } else {
            resultsContainerView.hidden = false
        }
        
        switch textFieldType {
        case .StartLocationTextField:
            //print("Edited start location textfield: \(textField.text)")
            buildingsManager.startBuildingsSearchResults = buildingsManager.searchBuildingsWithString(textField.text!)
            resultsTableView.reloadData()
            
        case .FinishLocationTextField:
            //print("Edited finish location textfield: \(textField.text)")
            buildingsManager.finishBuildingsSearchResults = buildingsManager.searchBuildingsWithString(textField.text!)
            resultsTableView.reloadData()
        }
    }
    
    func calculateRouteFrom(startLocation: CLLocationCoordinate2D, to finishLocation: CLLocationCoordinate2D, routeType: RouteType) {
        RoutesManager.calculateRouteFrom(startLocation, to: finishLocation, routeType: routeType) { result in
            switch result {
            case .success(let route):
                for i in 0..<self.polylinesArray.count {
                    let polyline = self.polylinesArray[i]
                    polyline.map = nil
                }
                
                print("\(self.dynamicType) - Got the polyline!")
                self.polylinesArray.append(route.polyline)
                route.polyline.map = self.mapView
                
                self.setDistanceAndEtaLabelForRoute(route)
                
            case .Failure(let errorType):
                print("\(self.dynamicType) - Error gettin driving routes: \(errorType)")
            }
        }
    }
    
    func setDistanceAndEtaLabelForRoute(route: Route) {
        var string = ""
        print("distance: \(route.distance)")
        print("eta: \(route.estimatedTime)")
        if let distanceText = route.distance { string = string + "Distance: " + distanceText + "     " }
        if let estimatedTimeText = route.estimatedTime { string = string + "ETA: " + estimatedTimeText }
        distanceAndEtaLabel.text = string
    }
    
    //MARK: Actions 
    
    func panGestureDetected(gesture: UIGestureRecognizer) {
        //print("Detected panning")
        switch gesture.state {
        case .Began:
            print("Panning began")
            self.view.endEditing(true)
            UIView.animateWithDuration(0.5, delay: 0.0, options: .CurveEaseOut, animations: {
                self.formContainerView.transform = CGAffineTransformMakeTranslation(0.0, -(self.formContainerView.bounds.size.height - 37.0))
                }, completion: nil)
            
        case .Ended:
            print("Panning ended")
            UIView.animateWithDuration(0.5, delay: 0.0, options: .CurveEaseOut, animations: {
                self.formContainerView.transform = CGAffineTransformIdentity
                }, completion: nil)
            
        default:
            break
        }
    }
    
    @IBAction func routeButtonTapped(sender: UIButton) {
        guard let routeButtonType = RouteButtonType(rawValue: sender.tag) else { return }
        switch routeButtonType {
        case .WalkingButton:
            RoutesManager.currentlySelectedRouteType = .Walking
            walkingButton.backgroundColor = .merckBlueColor()
            drivingButton.backgroundColor = .lightGrayColor()
            
        case .DrivingButton:
            RoutesManager.currentlySelectedRouteType = .Driving
            walkingButton.backgroundColor = .lightGrayColor()
            drivingButton.backgroundColor = .merckRedColor()
        }
        
        performSearch()
        resultsContainerView.hidden = true
    }
    
    func performSearch() {
        if let startBuilding = buildingsManager.getStartBuilding(), let finishBuilding = buildingsManager.getFinishBuilding() where startBuilding != finishBuilding {
            print("Will calculate route")
            calculateRouteFrom(buildingsManager.getStartBuilding()!.entranceLocation, to: buildingsManager.getFinishBuilding()!.entranceLocation, routeType: RoutesManager.currentlySelectedRouteType)
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////

extension MapViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let currentlyActiveSearch = buildingsManager.currentlyActiveSearch else { return 0 }
        switch currentlyActiveSearch {
        case .StartBuilding:
            return buildingsManager.startBuildingsSearchResults.count
        case .FinishBuilding:
            return buildingsManager.finishBuildingsSearchResults.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BuildingCell", forIndexPath: indexPath)
        
        if let currentlyActiveSearch = buildingsManager.currentlyActiveSearch {
            switch currentlyActiveSearch {
            case .StartBuilding:
                cell.textLabel?.text = buildingsManager.startBuildingsSearchResults[indexPath.row].name
            case .FinishBuilding:
                cell.textLabel?.text = buildingsManager.finishBuildingsSearchResults[indexPath.row].name
            }
        }
        
        return cell
    }
}

extension MapViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        view.endEditing(true)
        if let CurrentlyActiveSearch = buildingsManager.currentlyActiveSearch {
            let selectedBuilding: Building
            let fillColor: UIColor
            
            switch CurrentlyActiveSearch {
            case .StartBuilding:
                selectedBuilding = buildingsManager.startBuildingsSearchResults[indexPath.row]
                startLocationTextField.text = selectedBuilding.name
                buildingsManager.setStartBuilding(selectedBuilding)
                drawingManager.currentStartPolygon?.fillColor = .darkGrayColor()
                drawingManager.currentStartPolygon = drawingManager.getPolygonOfBuilding(selectedBuilding)
                fillColor = .merckLogoBlueColor()
                
            case .FinishBuilding:
                selectedBuilding = buildingsManager.finishBuildingsSearchResults[indexPath.row]
                finishLocationTextField.text = selectedBuilding.name
                buildingsManager.setFinishBuilding(selectedBuilding)
                drawingManager.currentFinishPolygon?.fillColor = .darkGrayColor()
                drawingManager.currentFinishPolygon = drawingManager.getPolygonOfBuilding(selectedBuilding)
                fillColor = .merckGreenColor()
            }
          
            if let selectedPolygon = drawingManager.getPolygonOfBuilding(selectedBuilding) {
                drawingManager.paintPolygon(selectedPolygon, withColor: fillColor)
            }
            
            mapView.animateToCameraPosition(GMSCameraPosition.cameraWithLatitude(selectedBuilding.entranceLocation.latitude, longitude: selectedBuilding.entranceLocation.longitude, zoom: 16.0))
            performSearch()
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        resultsContainerView.hidden = true
    }
}

extension MapViewController: GMSMapViewDelegate {
    func mapView(mapView: GMSMapView, didTapOverlay overlay: GMSOverlay) {
        print("Tapping overlayyy")
        if overlay is GMSPolygon {
            print("Did tap building with name: \(overlay.title)")
            let selectedBuilding = buildingsManager.buildings.filter({ building in
                building.name == overlay.title
            }).first!
            
            print("buildings coors: \(selectedBuilding.entranceLocation)")
            let alertController = UIAlertController(title: "", message: "Select \(selectedBuilding.name) as:", preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "Start", style: .Default, handler: { _ in
                self.buildingsManager.setStartBuilding(selectedBuilding)
                self.startLocationTextField.text = selectedBuilding.name
                if let selectedPolygon = self.drawingManager.getPolygonOfBuilding(selectedBuilding) {
                    self.drawingManager.currentStartPolygon?.fillColor = .darkGrayColor()
                    self.drawingManager.currentStartPolygon = self.drawingManager.getPolygonOfBuilding(selectedBuilding)
                    self.drawingManager.paintPolygon(selectedPolygon, withColor: .merckLogoBlueColor())
                }
                self.performSearch()
            }))
            alertController.addAction(UIAlertAction(title: "Finish", style: .Default, handler: { _ in
                self.buildingsManager.setFinishBuilding(selectedBuilding)
                self.finishLocationTextField.text = selectedBuilding.name
                if let selectedPolygon = self.drawingManager.getPolygonOfBuilding(selectedBuilding) {
                    self.drawingManager.currentFinishPolygon?.fillColor = .darkGrayColor()
                    self.drawingManager.currentFinishPolygon = self.drawingManager.getPolygonOfBuilding(selectedBuilding)
                    self.drawingManager.paintPolygon(selectedPolygon, withColor: .merckGreenColor())
                }
                self.performSearch()
            }))
            alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
            presentViewController(alertController, animated: true, completion: nil)
        }
    }
}

extension MapViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        guard let textFieldType = TextFieldType(rawValue: textField.tag) else { return }
        switch textFieldType {
        case .StartLocationTextField:
            buildingsManager.currentlyActiveSearch = .StartBuilding
        case .FinishLocationTextField:
            buildingsManager.currentlyActiveSearch = .FinishBuilding
        }
        locationTextFieldEdited(textField)
    }
}
