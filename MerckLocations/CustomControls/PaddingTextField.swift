//
//  PaddingTextField.swift
//  MerckLocations
//
//  Created by Diego Vidal on 26/04/16.
//  Copyright © 2016 Diego Vidal. All rights reserved.
//

import UIKit

class PaddingTextField: UITextField {

    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds, 30.0, 0.0)
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds, 30.0, 0.0)
    }
    
    override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds, 30.0, 0.0)
    }
}
